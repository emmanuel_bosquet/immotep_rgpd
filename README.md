Groupe 3 : site internet vitrine avec possibilité de demander à être rappelé sur son portable pour un rendez-vous par l’agence la plus proche du lieu où on se trouve au moment de la demande 


# Immotep, mentions légales de protection des données personnelles

## Contexte

Immotep est notre client, qui nous a demandé une appli qui permet aux chercheurs de bien immobilier d'être rappelés par un agent de l'agence la plus proche.

## Les mentions légales

Immotep.fr s'engage à respecter le RGPD (Réglement Général de Protection des Données)
ainsi que la loi Informatique et Liberté de 1978.

### 1. Définition et nature des données

Les données que nous collectons:

- Nom
- Prénom
- Téléphone
- email (optionnel)
- localisation
- critères de recherches de bien immobilier (budget, zone géographique, type de bien, type de bail)

### 2. Droit d'accès

Pendant la durée de votre contrat, vous disposez d'un droit d'accès à vos données, en lecture, et en rectification.

Vos données seront automatiquement supprimées un an après la date d'échéance de votre dernier contrat.
Vous pouvez demander la suppression anticipée de vos données à partir de la fin de votre contrat. 

Pour ces différentes options, vous pouvez contacter immédiatement votre agent immobilier ou passer par le formulaire en ligne sur <immotep.fr>.

### 3. Gestion de vos données personnelles

#### Moyen de collecte

Les données sont collectées par formulaire sur <immotep.fr>

#### Finalité de la collecte des données

Les données sont collectées dans le but principal de fournir un service adapté à la demande de l'utilisateur.

Nous pouvons également nous en servir pour prospecter (proposer des biens), à condition que l'utilisateur y consente.

#### Conservation des données

Les données sont conservées dans une base de données hébergée chez Clever Cloud,
hébergeur français militant qui stocke les données dans des Data Center à Paris.


#### Destinataires des données

Les données sont destinées :

1. en totalité, à l'agent immobilier de l'agence choisie par le formulaire, en fonction de la localisation, pour prendre contact avec le client.
2. en partie (tout sauf le numéro de téléphone), au département de prospection de l'agence, pour envoyer des annonces personnalisées par courriel.

Les données ne sont transmises à aucun tiers collaborateur d'Immotep.


### Auteurs

- Théo Helaine
- Jean-Michel Dumont
- Emmanuel Bosquet
